const DigiByte = require("digibyte");
const request = require("request");
var Mnemonic = require('digicore-mnemonic');

class DigiByteUtils {
    
    constructor(network) {
        this.networks =  ['testnet', 'mainnet'];
        this.marketUrl = "https://api.coinmarketcap.com/v1/ticker";
        this.network = network.toLowerCase();
        if(this.networks.indexOf(this.network) > -1) {
            this.explorerUrl = "https://"+((this.network == this.networks[0]) ?  'testnet.' : '') +"digiexplorer.info";
            
            DigiByte.Networks.defaultNetwork = DigiByte.Networks[this.network];  
            console.log(DigiByte.Networks.defaultNetwork.dnsSeeds);          
        } else {
        	throw new Error('Network invalid, you should  only use "testnet" and "mainnet" as parameter');
        }
    }

    generateHDWallet(mnemonic) {
        var p = null;
        if(mnemonic != undefined) {
            p = new Mnemonic(mnemonic);
        } else {
            p = new Mnemonic();
            
        }

        let hdPrivateKey = new DigiByte.HDPrivateKey(p.toHDPrivateKey().toString());
        let privateKey = hdPrivateKey.deriveChild("m/44'/0'/0'/0/0'").privateKey;
        let publicKey = this.generatePublicKey(privateKey);
        let address = new DigiByte.Address(publicKey);
        return {
            address : address.toString(),
            publicKey : publicKey.toString(),
            privateKey: privateKey.toWIF(),
            account: privateKey.toAddress().toString(),
            seed : p.toString()
        }
    }

    getBalance(address) {
        return new Promise((resolve, reject) => {
            request.get(this.explorerUrl+'/api/addr/'+address, function (error, response, body) {
                if(error) {
                    reject(error);
                }
                console.log(body)
                resolve(JSON.parse(body));
            });    
        })
    }

    validateAddress(address) {
        return DigiByte.Address.isValid(address, this.network);
    }

    validatePrivateKey(privateKey) {
        return DigiByte.PrivateKey.isValid(privateKey, this.network);
    }

    validatePublicKey(publicKey) {
        return DigiByte.PublicKey.isValid(publicKey, this.network);
    }

    validateMnemonic(mnemonic) {
        return Mnemonic.isValid(mnemonic);
    }

    generatePrivateKey() {
        return new DigiByte.PrivateKey(DigiByte.Networks.defaultNetwork);
    }

    getPrivateKey(wif) {
        return  DigiByte.PrivateKey.fromWIF(wif);
    }


    generatePublicKey(privateKey) {
        return  DigiByte.PublicKey(privateKey);
    }

    generateAccount(publicKey) {
        return  new DigiByte.Address(publicKey)
    }

    getAccount(address) {
        return DigiByte.Address.fromString(address)
    }

    getUnspentTransactionOutput(address) {
        return new Promise((resolve, reject) => {
            request.get(this.explorerUrl + "/api/addr/" + address + "/utxo", (error, response, body) => {
                if(error) {
                  reject(error);
                }
                resolve(JSON.parse(body));
            });
        });
    }

    getTransacctionFromRawTx(rawtx) {
        return new DigiByte.Transaction(rawtx);
    }

    createTransacction(privateKey,account,destinationAddress, satoshis,fee) {
        return new Promise((resolve, reject) => {
             this.getUnspentTransactionOutput(account.toString()).then(utxos => {
                if(utxos.length == 0) {
                    reject({ "message": "The source address has no unspent transactions" });
                }  
                
                let transaction = new DigiByte.Transaction();
                let unspentOutputs = []; 
                let simpleUtxo;
                for(var i = 0; i < utxos.length; i++) {
                    simpleUtxo = {
                        address: account.toString(),
                        txid: utxos[i].txid,
                        outputIndex: 0,
                        script  :  DigiByte.Script.fromAddress(privateKey.publicKey.toAddress()),
                        satoshis : DigiByte.Unit.fromDGB(1).toSatoshis(),
                    };
                }    
                    let UTXO = new DigiByte.Transaction.UnspentOutput(simpleUtxo);
                    transaction.from(UTXO);
                    if(fee > 667) {
                        transaction.fee(fee);
                    }
                    transaction.to(destinationAddress,satoshis)
                    .change(account)
                    .sign(privateKey);
                    
                resolve({transacction : transaction });
            })
        });
    }
    
    sendTransaction(transaction) {
        return new Promise((resolve, reject) => {
          request.post({
                "headers": { "content-type": "application/json" },
                "url": this.explorerUrl + "/api/tx/send",
                "body": JSON.stringify({
                    "rawtx": ""+transaction
                })
            }, (error, response, body) => {
                if(error) {
                   console.log('error ::', error);
                    reject(error);
                }
                console.log(body);
                resolve(JSON.parse(body));
            });
        });
    }
}

exports.DigiByteUtils = DigiByteUtils;