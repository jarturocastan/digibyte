const { check } = require('express-validator');



class Validations {
    onlyAddress(digiByteUtils,paramName = 'address') {   
        return [
            check(paramName).exists().withMessage(paramName+' is required').isString().withMessage(paramName+' should be a string value')
            .isLength(34).withMessage(paramName+' should have 34 characters').custom((value, { req }) => {
                if(digiByteUtils.validateAddress(value) == false) {
                    throw new Error(paramName+' is invalid');
                }
                return true;
            })
        ]
    }

    onlyPrivateKey(digiByteUtils,paramName = 'privateKey') {
        return [
            check(paramName).exists().withMessage(paramName+' is required').isString().withMessage(paramName+' should be a string value')
            .isLength(52).withMessage(paramName+' should have 52 characters').custom((value, { req }) => {
                if(digiByteUtils.validatePrivateKey(value) == false) {
                    throw new Error(paramName+' is invalid');
                }
                return true;
            })
        ]
    }

    onlyPublicKey(digiByteUtils,paramName = 'publicKey') {
        return [
            check(paramName).exists().withMessage(paramName+' is required').isString().withMessage(paramName+' should be a string value')
            .isLength(44).withMessage(paramName+' should have 44 characters').custom((value, { req }) => {
                if(digiByteUtils.validateAddress(value) == false) {
                    throw new Error(paramName+' is invalid');
                }
                return true;
            })
        ]
    }

    onlyAmount(paramName = 'amount') {
        return [
            check(paramName).exists().withMessage(paramName+' is required').isInt().withMessage(paramName+' should be a integer value')
            .custom((value, { req }) => {
                if(value <= 0) {
                    throw new Error(paramName+' should be more than 0');
                }
                return true;
            })
        ]
    }

    onlyMnemonic(digiByteUtils,paramName = 'mnemonic') {
        return [
            check(paramName)
            .custom((value, { req }) => {
                
                if(value === null || value === '' || value == undefined) {
                    return true;
                } else {
                    if(digiByteUtils.validateMnemonic(value) == false) {
                        throw new Error(paramName+' is invalid');
                    }
                    return true;
                }
            })
        ]
    }

    onlyFee(paramName = 'fee') {
        return [
            check(paramName)
            .custom((value, { req }) => {
                if(value === null || value === '' || value == undefined) {
                    return true;
                } else {
                    let fee = parseInt(value);
                    if(fee <= 667) {
                        throw new Error(paramName+' must be more than 0');
                    }
                    return true;
                }
            })
        ]
    }

    onlyRAWTx(paramName = 'rawtx') {
        return [
            check(paramName)
            .custom((value, { req }) => {
               
                return true;
                
            })
        ]
    }

}

module.exports.Validations = Validations;
    


