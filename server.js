const express = require("express")
const request = require('request');
const dotenv = require('dotenv');
const { check, validationResult } = require('express-validator');
const {DigiByteUtils}  = require("./DigibyteUtils");
const { Validations }  = require("./Validations");

dotenv.config();
let app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let digiByteUtils = new DigiByteUtils((process.env.NODE_ENV == 'developer') ? 'testnet': 'mainnet');
let validations = new Validations();

app.get('/', function (req, res) {
  res.send('REST API wallet dash with your address');
});

//You can pass a seed on the param mnemonic and get de wallet associateld if you dont pass it, you generate a new 
//wallet with a new seed 
//@Release
app.get('/generate-wallet',validations.onlyMnemonic(digiByteUtils), function(req,res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  var mnemonic =  req.body.mnemonic;  
  res.send(digiByteUtils.generateHDWallet(mnemonic));
})


//@release
app.get('/balance',validations.onlyAddress(digiByteUtils), function(req, res) { 
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let address = req.body.address;
  digiByteUtils.getBalance(address).then(success => {
    res.send({ satoshis : success.balanceSat});
  }, error => {
    return res.status(422).json({ error : error });
  })
}); 


//@release
app.get('/utxo',validations.onlyAddress(digiByteUtils), function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let address = req.body.address;
  digiByteUtils.getUnspentTransactionOutput(address).then(success => {
    res.send(success);
  }, error => {
    return res.status(422).json({ error : error });
  })
});



let feeValidation = validations.onlyPrivateKey(digiByteUtils);
feeValidation = feeValidation.concat(validations.onlyAddress(digiByteUtils,'to'));
feeValidation = feeValidation.concat(validations.onlyAmount());

//@release
app.get('/fee',feeValidation, function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let amount = Number(req.body.amount);
  let toAddress = req.body.to;
  let privateKey = req.body.privateKey;
  privateKey = digiByteUtils.getPrivateKey(privateKey);
  let publicKey = digiByteUtils.generatePublicKey(privateKey);
  let account = digiByteUtils.generateAccount(publicKey);
  digiByteUtils.createTransacction(privateKey,account,toAddress,amount)
    .then(success => {
      res.send({ fee : success['transacction'].getFee()});
    }, error => {
      return res.status(422).json({ error : error });
    })
});

feeValidation = feeValidation.concat(validations.onlyFee());

//@release
app.post('/sign', feeValidation, function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let amount = Number(req.body.amount);
  let toAddress = req.body.to;
  let privateKey = req.body.privateKey;
  let fee = Number(req.body.fee);
  privateKey = digiByteUtils.getPrivateKey(privateKey);
  let publicKey = digiByteUtils.generatePublicKey(privateKey);
  let account = digiByteUtils.generateAccount(publicKey);
  digiByteUtils.createTransacction(privateKey,account,toAddress,amount,fee)
    .then(success => {
     return  res.send({ txraw : success['transacction'].serialize()});
    }, error => {
      return res.status(422).json({ error : error });
    })
});


//@release
app.post('/broadcast',validations.onlyRAWTx(),function(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  let rawtx = req.body.rawtx;
  let transacction = digiByteUtils.getTransacctionFromRawTx(rawtx);
  digiByteUtils.sendTransaction(transacction).then(success =>{
    return res.send(success);
  }, error => {
    return res.status(422).json({ error : error });
  })

});

app.listen(3000, () => {
  console.log("The server is running on port 3000");
});